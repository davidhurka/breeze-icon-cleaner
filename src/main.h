/***************************************************************************
 *   Copyright (C) 2020 by David Hurka <david.hurka@mailbox.org>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#ifndef MAIN_H
#define MAIN_H

#include <QList>

class QSvgRenderer;
class QRect;
class QColor;

class IconColorScheme;

// Command line parsing
/**
 * Splits @p sizesArg in single icon sizes.
 * If invalid sizes were specified, or sizes are not ascending, returns an empty list.
 */
QList<int> getSizes(const QString &sizesArg);

/**
 * Splits @p namesArg in single icon names.
 * If duplicate names were specified, returns an empty list.
 * For items to skip, adds @c QString() to the return list.
 */
QStringList getNames(const QString &namesArg);

/**
 * Returns a valid icon count or 0 for @p countArg.
 */
int getCount(const QString &countArg);

/**
 * Parses --classes arguments @p classesArg,
 * and adds the classes to @p colorScheme.
 * If invalid or duplicate colors were specified, returns false.
 */
bool getClasses(IconColorScheme &colorScheme, const QString &classesArg);

/**
 * Parses --proxy-colors arguments @p colorsArg,
 * and adds the proxy colors to @p colorScheme.
 * If invalid or duplicate colors were specified, returns false.
 */
bool getProxyColors(IconColorScheme &colorScheme, const QString &colorsArg);

bool useExtremeCssClassNames;

/**
 * Checks whether @p className is a valid CSS class name.
 * Respects @c useExtremeCssClassNames;
 * If invalid, prints an error messages and returns false.
 */
bool isValidCssClass(const QString &className);

/**
 * Parses --fixed-colors arguments @p colorsArg,
 * and adds the fixed colors to @p colorScheme.
 * If invalid or duplicate colors were specified, returns false.
 */
bool getFixedColors(IconColorScheme &colorScheme, const QString &colorsArg);

/**
 * Returns the specified file name from positional arguments @p fileNameArgs.
 * If multiple or no positional arguments were given, returns QString().
 */
QString getFileName(const QStringList &fileNameArgs);

/**
 * Tries to create the directory @p outputDirArg.
 * If successful, it returns the path, otherwise QString().
 */
QString getOutputDir(const QString &outputDirArg);

/**
 * Calculate the vertical offset for every icon size.
 * E. g. icon sizes 16, 24, 32 will have offsets 0, 16, 40.
 */
QList<int> getVerticalOffsets(const QList<int> &sizes);

/**
 * Generate the template file with one square for every icon and icon size.
 * Generate swatches for every color in @p colorScheme.
 * Opaque colors
 */
void generateTemplate(const QList<int> &sizes,
                      const QList<int> &verticalOffsets, int count,
                      const IconColorScheme &colorScheme,
                      const QString &outputFileName);
QString generateSquare(int size, int offsetX, int offsetY,
                       const QColor &color);

/**
 * Adds the 5 breeze monochrome classes to @p colorScheme.
 */
void fillColorSchemeBreeze(IconColorScheme &colorScheme);

/**
 * Converts one region of the input file to a breeze icon,
 * by piping it through QSvgRenderer and IconPaintDevice.
 *
 * @par renderer An QSvgRenderer which has loaded the input file.
 * @par clip Position and size of the icon in the input file.
 * @par colorScheme The icon color scheme with the breeze color loaded.
 * @par outputFileDir The output directory, e. g. "16".
 * @par outputFileName the output file name, e. g. "icon.svg".
 */
void generateIcon(QSvgRenderer *renderer,
                  const QRect &clip,
                  const IconColorScheme *colorScheme,
                  const QString &outputFileDir,
                  const QString &outputFileName);

#endif // MAIN_H
