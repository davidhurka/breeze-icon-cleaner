/***************************************************************************
 *   Copyright (C) 2019-2020 by David Hurka <david.hurka@mailbox.org>      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#include "iconpaintdevice.h"

#include "iconpaintengine.h"

IconPaintDevice::IconPaintDevice()
    : QPaintDevice()
    , m_paintEngine(new IconPaintEngine())
{
}

IconPaintDevice::~IconPaintDevice()
{
    delete m_paintEngine;
}

QPaintEngine *IconPaintDevice::paintEngine() const
{
    return m_paintEngine;
}

void IconPaintDevice::setClip(const QRect clip)
{
    m_clip = clip;
    m_paintEngine->setClip(clip);
}

bool IconPaintDevice::save(QIODevice* device)
{
    return m_paintEngine->save(device);
}

int IconPaintDevice::metric(PaintDeviceMetric metric) const
{
    if (metric == PdmWidth) {
        return m_clip.width();
    } else if (metric == PdmHeight) {
        return m_clip.height();
    } else if (metric == PdmDevicePixelRatio) {
        return 1;
    } else if (metric == PdmDpiX) {
        return 72;
    } else if (metric == PdmDpiY) {
        return 72;
    } else {
        return QPaintDevice::metric(metric);
    }
}

