/***************************************************************************
 *   Copyright (C) 2019-2020 by David Hurka <david.hurka@mailbox.org>      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#include "iconcolorscheme.h"

IconColorScheme::IconColorScheme()
{
}

IconColorScheme::~IconColorScheme()
{
}

bool IconColorScheme::addEntry(const Entry &newEntry)
{
    // Make sure that no two entries with same input colors are added
    for (Entry e : m_entries) {
        if (e.inputColor == newEntry.inputColor) {
            return false;
        }
    }

    // Make sure that no two BreezeClass entries for same class are added
    if (newEntry.type == BreezeClass) {
        for (Entry e : m_entries) {
            if (e.type == BreezeClass && e.className == newEntry.className) {
                return false;
            }
        }
    }

    m_entries.append(newEntry);
    return true;
}

const IconColorScheme::Entry * IconColorScheme::entryForClassName(const QString& className) const
{
    for (int i = m_entries.count() - 1; i >= 0; --i ) {
        if (m_entries.at(i).type == BreezeClass && m_entries.at(i).className == className) {
            return &m_entries.at(i);
        }
    }

    return nullptr;
}

const IconColorScheme::Entry * IconColorScheme::entryForInputColor(QColor inputColor) const
{
    // Compare inputColor with first entry
    int indexOfBestMatch = 0;
    int distanceOfBestMatch = colorDistance(m_entries.at(0).inputColor, inputColor);
    int countOfEqualMatches = 1;

    // Compare inputColor with all other entries
    for (int i = m_entries.count() - 1; i >= 1; --i) {
        int distance = colorDistance(m_entries.at(i).inputColor, inputColor);
        if (distance == distanceOfBestMatch) {
            ++countOfEqualMatches;
        } else if (distance < distanceOfBestMatch) {
            indexOfBestMatch = i;
            distanceOfBestMatch = distance;
            countOfEqualMatches = 1;
        }
    }

    if (countOfEqualMatches > 1) {
        return nullptr;
    } else {
        return &m_entries.at(indexOfBestMatch);
    }
}

const QVector<IconColorScheme::Entry> &IconColorScheme::allEntries() const
{
    return m_entries;
}

int IconColorScheme::colorDistance(QColor a, QColor b)
{
    // TODO replace by good distance calculation.
    return abs(a.red() - b.red()) + abs(a.green() - b.green()) + abs(a.blue() - b.blue());
}
