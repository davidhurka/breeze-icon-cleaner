/***************************************************************************
 *   Copyright (C) 2019-2020 by David Hurka <david.hurka@mailbox.org>      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#ifndef ICONPAINTDEVICE_H
#define ICONPAINTDEVICE_H

#include <QObject>
#include <QPaintDevice>

#include "iconcolorscheme.h"

class QIODevice;
class IconPaintEngine;

/**
 * This paint device lets QPainter paint on an IconPaintEngine.
 *
 * The IconPaintEngine stores the data in its own QDomDocument.
 * To write the final output SVG to a QIODevice, call save().
 *
 * @par Clip
 * The clip of this device determines which region of the input
 * SVG file will appear in the output SVG file.
 * This allows to split one input file into several icon output files.
 */
class IconPaintDevice : public QPaintDevice
{
public:
    explicit IconPaintDevice();
    ~IconPaintDevice();

    inline const IconColorScheme *iconColorScheme() const { return m_colorScheme; }
    inline void setIconColorScheme(const IconColorScheme *scheme)
    {
        m_colorScheme = scheme;
    }

    inline QRect clip() const { return m_clip; }
    /**
     * Set which region of the original drawing shall
     * appear in the new icon file.
     *
     * @note
     * Before rendering a QSvgRenderer on this device,
     * the QSvgRenderer view box must be set to the same
     * size as this clip.
     */
    void setClip(const QRect clip);

    QPaintEngine *paintEngine() const override;

    /**
     * Writes the QDomDocument to @p device.
     *
     * Fails when IconPaintEngine::end() was not called.
     */
    bool save(QIODevice *device);

protected:
    // TODO how do metric information work?
    virtual int metric(QPaintDevice::PaintDeviceMetric metric) const  override;
    IconPaintEngine *m_paintEngine;
    const IconColorScheme *m_colorScheme;
    QRect m_clip;
};

#endif // ICONPAINTDEVICE_H
