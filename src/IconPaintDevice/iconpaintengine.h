/***************************************************************************
 *   Copyright (C) 2019-2020 by David Hurka <david.hurka@mailbox.org>      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#ifndef ICONPAINTENGINE_H
#define ICONPAINTENGINE_H

#include "iconcolorscheme.h"

#include <QPaintEngine>
#include <QSet>
#include <QDomDocument>

/**
 * @short
 * Does the actual work to create the output SVG.
 *
 * When playing the QSvgRenderer to an IconPaintDevice,
 * this engine takes the path elements and writes them to a QDomDocument,
 * with class attributes as specified in the IconColorScheme.
 *
 * The only capability is QPaintEngine::PainterPaths,
 * because paths should be sufficient for monochrome icons.
 */
class IconPaintEngine : public QPaintEngine
{
    /**
     * Updated by begin(), end(), and updateState().
     * Needed to determine whether draw*() and save() are allowed.
     */
    enum State {
        StateEmpty,     ///< After construction.
        StateBegun,     ///< After begin(), needs updateState() now.
        StateReady,     ///< After first updateState(), can be draw*()n now.
        StateComplete   ///< After end(), can be save()d now.
    };

public:
    explicit IconPaintEngine();
    ~IconPaintEngine();

    QPaintEngine::Type type() const override;

    /**
     * Initialize the QDomDocument.
     *  * SVG root element
     *  * Icon size
     */
    bool begin(QPaintDevice *pdev) override;

    /**
     * Adds the stylesheet to the QDomDocument.
     */
    bool end() override;

    /**
     * The core of this class. :)
     */
    void drawPath(const QPainterPath &path) override;

    /**
     * Makes error message, pixmaps are not wanted.
     */
    void drawPixmap(const QRectF &r, const QPixmap &pm, const QRectF &sr) override;

    /**
     * See QPaintEngineState detailed description,
     * probably interface for QPainter to change several parameters.
     */
    void updateState(const QPaintEngineState &state) override;

    /**
     * Writes the SVG representation of the QDomDocument to @p device.
     *
     * Called by IconPaintDevice::save().
     *
     * Fails when end() was not called.
     */
    bool save(QIODevice *device);

    // TODO what about override?
    inline bool hasFeature(QPaintEngine::PaintEngineFeature feature) const
    {
        return feature == QPaintEngine::PainterPaths;
    }

    inline void setClip(QRect clip) { m_clip = clip; }

protected:
    /**
     * Makes a coordinate pair string
     * as required for the d attribute of \<path\>.
     */
    static QString coordinatePair(qreal x, qreal y);

    /**
     * Converts a QPainterPath to an svg path data string,
     * as it can be used for the @c d attribute of a @c <path/> element.
     */
    static QString pathDataToString(const QPainterPath &path);

    QDomDocument m_outputDocument;
    QDomElement m_documentElement;

    /**
     * The @c <devs> element of the output SVG.
     * Note that it is created at begin(), to end up in the beginning of the output file.
     */
    QDomElement m_defsElement;

    /**
     * To remember which classes were used in the icon.
     * These classes will be included in the icon's stylesheet.
     */
    QSet<QString> m_usedClasses;

    /**
     * The style which is used for the next element.
     * It is determined by the color passed to updateState().
     */
    const IconColorScheme::Entry *m_currentStyle;

    /**
     * A fallback style for the case no or an invalid style was set by updateState().
     */
    IconColorScheme::Entry m_emptyStyle;

    /**
     * Controls what can be done with this engine at any time.
     */
    State m_state;

    /**
     * Controls to which region the input image is clipped.
     */
    QRect m_clip;
};

#endif // ICONPAINTENGINE_H
