/***************************************************************************
 *   Copyright (C) 2019-2020 by David Hurka <david.hurka@mailbox.org>      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#ifndef ICONCOLORSCHEME_H
#define ICONCOLORSCHEME_H

#include <QColor>
#include <QVector>

/**
 * A collection of element styles, including breeze class name, color, transparency.
 *
 * Used to match paths from icon input files to styles,
 * and for generating the stylesheets in the icon output files.
 *
 * @par Draft colors
 * Draft colors can be specified to remove elements from the icon.
 *
 * @par Fixed colors
 * Fixed colors can be specified to exclude
 * elements from the breeze stylesheet.
 * Instead, they will get a fill attribute with the specified output color.
 * Then, if needed, their style can be edited manually in the output file.
 * This feature makes it easier to find elements to be modified.
 *
 * @par Proxy Colors
 * Proxy colors can be used to link elements from the input file
 * to other classes or different fixed output colors.
 * Additionally, they allow to specify transparency.
 *
 * @note
 * This color scheme covers only solid path fills, no gradients or stroke.
 * If you need such features,
 * you can use a fixed color to mark the corresponding elements,
 * and add the features manually in the output file.
 */
class IconColorScheme
{
public:
    /**
     * Type of color scheme entry.
     */
    enum Type {
        BreezeClass,        ///< Used to map elements from the input file directly to a breeze class. Only these entries define the output stylesheet.
        BreezeClassProxy,   ///< Used to map elements from the input file to a breeze class of other style. This is needed e. g. to define transparent, CSS styled elements.
        DraftColor,         ///< Used to discard elements from the input file.
        FixedColor          ///< Used to give elements from the input file a fixed color, instead of mapping them to a breeze class.
    };

    /**
     * One entry in the color scheme.
     * Elements from the input file will be mapped to one @c Entry using entryForInputColor().
     *
     * Entries can be added with addEntry(),
     * and fetched with entryForClassName() and entryForInputColor().
     */
    struct Entry {
        QColor inputColor;          ///< Determines which elements from the input file will be mapped to this entry.

        QColor outputColor;         ///< For BreezeClass entries, this defines what will be written to the stylesheet. For FixedColor entries, this defines the fixed color.
        QString className;          ///< To which class name the element will be linked, if applicable.
        qreal transparency;         ///< Defines the transparency in the output file. 0..1 for invisible..opaque.
        Type type;
    };

    explicit IconColorScheme();
    ~IconColorScheme();

    /**
     * Adds an entry.
     *
     * Fails if already one entry with the same input color present.
     * When adding a BreezeClass entry, fails if the class name is already present.
     */
    bool addEntry(const Entry &newEntry);

    /**
     * Does a color distance calculation for all known colors,
     * and returns the @c entry of the best match.
     *
     * If two entries match equal, returns nullptr.
     */
    const Entry *entryForInputColor(QColor inputColor) const;

    /**
     * Returns the BreezeClass entry with class name @p className.
     *
     * If no such entry present, returns nullptr.
     */
    const Entry *entryForClassName(const QString &className) const;

    const QVector<Entry> &allEntries() const;

protected:
    QVector<Entry> m_entries;

    /**
     * Returns manhattan distance between @p a and @p b in RGB color model.
     */
    static int colorDistance(QColor a, QColor b);
};

#endif // ICONCOLORSCHEME_H
