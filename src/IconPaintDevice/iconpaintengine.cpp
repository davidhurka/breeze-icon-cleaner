/***************************************************************************
 *   Copyright (C) 2019-2020 by David Hurka <david.hurka@mailbox.org>      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#include "iconpaintengine.h"

#include <QDebug>
#include <QObject>
#include <QPainterPath>

#include "iconpaintdevice.h"
#include "iconcolorscheme.h"

IconPaintEngine::IconPaintEngine()
    : QPaintEngine(QPaintEngine::PainterPaths)
    , m_currentStyle(&m_emptyStyle)
    , m_emptyStyle{Qt::transparent, Qt::black, QString(), 1, IconColorScheme::FixedColor}
    , m_state(StateEmpty)
{
}

IconPaintEngine::~IconPaintEngine()
{
}

QPaintEngine::Type IconPaintEngine::type() const
{
    return QPaintEngine::SVG;
}

bool IconPaintEngine::begin(QPaintDevice* pdev)
{
    Q_UNUSED(pdev)

    // Clear the QDomDocument and initialize it with icon metadata.
    m_outputDocument = QDomDocument("svg");
    m_documentElement = m_outputDocument.createElement("svg");
    m_documentElement.setAttribute("viewBox", QStringLiteral("0 0 %1 %2")
                                              .arg(QString::number(m_clip.width()))
                                              .arg(QString::number(m_clip.height())));
    m_documentElement.setAttribute("version", "1.1");
    m_documentElement.setAttribute("xmlns", "http://www.w3.org/2000/svg");
    m_outputDocument.appendChild(m_documentElement);

    m_defsElement = m_outputDocument.createElement("defs");
    m_documentElement.appendChild(m_defsElement);

    m_state = StateBegun;

    return true;
}

bool IconPaintEngine::end()
{
    // Require output document to be begun (begin() called).
    if (!(m_state == StateBegun || m_state == StateReady)) {
        return false;
    }

    if (!m_usedClasses.isEmpty()) {
        // Create stylesheet element with nice formatting.
        const IconColorScheme *colorScheme
            = static_cast<IconPaintDevice*>(paintDevice())->iconColorScheme();
        QString stylesheet;
        stylesheet.prepend(QStringLiteral("\n"));

        for (QString className : m_usedClasses) {
            const IconColorScheme::Entry *classEntry = colorScheme->entryForClassName(className);
            QColor classColor;
            if (classEntry) {
                classColor = classEntry->outputColor;
            } else {
                qDebug() << "For the given class name" << className << "there is no entry in the color scheme.";
                classColor = Qt::black;
            }

            stylesheet.append(QStringLiteral(
                "            .%1 {\n"
                "                color:%2;\n"
                "            }\n"
                )
                .arg(className)
                .arg(classColor.name())
            );
        }

        stylesheet.append(QStringLiteral("        "));

        QDomElement s = m_outputDocument.createElement("style");
        s.setAttribute("type", "text/css");
        s.setAttribute("id", "current-color-scheme");
        QDomText t = m_outputDocument.createTextNode(stylesheet);
        s.appendChild(t);
        m_defsElement.appendChild(s);
    } else {
        // If no stylesheet needed, also remove <defs> element.
        m_documentElement.removeChild(m_defsElement);
    }

    m_state = StateComplete;

    return true;
}

void IconPaintEngine::updateState(const QPaintEngineState& state)
{
    const IconColorScheme *colorScheme
        = static_cast<IconPaintDevice*>(paintDevice())->iconColorScheme();
    m_currentStyle = colorScheme->entryForInputColor(state.brush().color());
    if (!m_currentStyle) {
        qDebug() << "Error: Found two color scheme entries with same color distance to input color.";
        m_currentStyle = &m_emptyStyle;
    }

    if (m_state == StateBegun) {
        m_state = StateReady;
    }
}


bool IconPaintEngine::save(QIODevice* device)
{
    if (m_state != StateComplete) {
        return false;
    }

    return device->write(m_outputDocument.toByteArray(4));
}

void IconPaintEngine::drawPath(const QPainterPath& path)
{
    if (m_state != StateReady) {
        return;
    }

    // Discard elements in draft color
    if (m_currentStyle->type == IconColorScheme::DraftColor) {
        return;
    }

    // Clip the input image
    QPainterPath translatedPath
        = path.translated(-m_clip.left(), -m_clip.top());
    QPainterPath clipRect;
    clipRect.addRect(0, 0, m_clip.width(), m_clip.height());
    QPainterPath clippedPath = translatedPath.intersected(clipRect);

    QString pathDataString = pathDataToString(clippedPath);
    if (pathDataString.isEmpty()) {
        return;
    }

    // Add path element to DOM
    QDomElement e = m_outputDocument.createElement("path");
    e.setAttribute("d", pathDataString);
    QStringList styleAttributes;
    styleAttributes << QStringLiteral("fill-opacity:%1").arg(m_currentStyle->transparency, 0, 'g', 2);
    styleAttributes << QStringLiteral("stroke:none");
    if (path.fillRule() == Qt::OddEvenFill) {
        styleAttributes << QStringLiteral("fill-rule:evenodd");
    }
    if (m_currentStyle->type == IconColorScheme::FixedColor) {
        // Fixed color element
        styleAttributes << QStringLiteral("fill:%1").arg(m_currentStyle->outputColor.name());
    } else {
        // Breeze class element
        styleAttributes << QStringLiteral("fill:currentColor");
        e.setAttribute(QStringLiteral("class"), m_currentStyle->className);
        m_usedClasses.insert(m_currentStyle->className);
    }
    e.setAttribute(QStringLiteral("style"), styleAttributes.join(QStringLiteral("; ")));
    m_outputDocument.documentElement().appendChild(e);
}

void IconPaintEngine::drawPixmap(const QRectF& r, const QPixmap& pm, const QRectF& sr)
{
    Q_UNUSED(pm)
    Q_UNUSED(sr)
    qDebug() << "Something tried to draw a pixmap on" << r;
}

QString IconPaintEngine::coordinatePair(qreal x, qreal y)
{
    if (!qIsFinite(x) || !qIsFinite(y)) {
        return "0 0";
    }

    // TODO Is using 'g' notation the perfect way?
    return QStringLiteral("%1 %2")
           .arg(QString::number(x, 'g'))
           .arg(QString::number(y, 'g'));
}

QString IconPaintEngine::pathDataToString(const QPainterPath &path)
{
    /*
     * How to parse QPainterPath data (in Qt 5.12):
     * Data element can be:
     * - MoveTo
     * - LineTo
     * - CurveTo
     * In case of CurveTo, following two elements will be CurveToData,
     * describing a cubic path segment.
     * The first two elements are the two control points,
     * the last element is the destination vertex.
     * (There are no quadratic path segments, QPainterPath::quadTo() just calls cubicTo().)
     */

    QString pathDataString;
    const int elementCount = path.elementCount();
    int segmentCount = 0;

    for (int i = 0; i < elementCount; ++i) {
        QPainterPath::Element vertex = path.elementAt(i);
        if (vertex.type == QPainterPath::ElementType::MoveToElement) {
            if (i != 0) {
                // Close last path loop
                pathDataString.append(QStringLiteral("Z "));
            }
            pathDataString.append(QStringLiteral("M "));
            pathDataString.append(coordinatePair(vertex.x, vertex.y));
            pathDataString.append(QStringLiteral(" "));
        } else if (vertex.type == QPainterPath::ElementType::LineToElement) {
            pathDataString.append(QStringLiteral("L "));
            pathDataString.append(coordinatePair(vertex.x, vertex.y));
            pathDataString.append(QStringLiteral(" "));
            ++segmentCount;
        } else if (vertex.type == QPainterPath::ElementType::CurveToElement) {
            if (i > elementCount - 3) {
                qDebug() << "BUG painter path ends with CurveToElement"
                    " with less than two CurveToDataElements.";
                return QString();
            }
            pathDataString.append(QStringLiteral("C "));
            pathDataString.append(coordinatePair(vertex.x, vertex.y));
            pathDataString.append(QStringLiteral(" "));
            vertex = path.elementAt(++i);
            pathDataString.append(coordinatePair(vertex.x, vertex.y));
            pathDataString.append(QStringLiteral(" "));
            vertex = path.elementAt(++i);
            pathDataString.append(coordinatePair(vertex.x, vertex.y));
            pathDataString.append(QStringLiteral(" "));
            ++segmentCount;
        }
    }
    pathDataString.append(QStringLiteral("Z "));

    if (segmentCount == 0) {
        // Nothing visible in the path.
        return QString();
    }

    return pathDataString.trimmed();
}

