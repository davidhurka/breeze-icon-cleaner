/***************************************************************************
 *   Copyright (C) 2019-2020 by David Hurka <david.hurka@mailbox.org>      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#include "main.h"

/*
 * This program cleans monocrome breeze icons.
 *
 * The user draws the icon in Inkscape (using only path elements).
 * This program reads the SVG and collects all path elements.
 * Then it writes them to another SVG file,
 * with the correct attributes (color scheme & CSS stuff).
 *
 * To aid drawing the icon, this program can generate templates,
 * on which multiple icons in multiple sizes can be drawn at once.
 * Then this program will separate these icons.
 *
 * Usage:
 * To get a template for two icons in three sizes:
 * $ breeze-icon-cleaner --sizes '16 24 32' --count 2 template.svg
 * To get the breeze icon files, after paining the icons on the template:
 * $ breeze-icon-cleaner --sizes '16 24 32' --names 'aaa bbb' template.svg
 */

#include <QApplication>
#include <QCommandLineParser>
#include <QRegularExpression>
#include <QSvgRenderer>
#include <QPainter>
#include <QDir>
#include <QFile>
#include <QDebug>

#include "IconPaintDevice/iconpaintdevice.h"
#include "IconPaintDevice/iconcolorscheme.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setApplicationName("breeze-icon-cleaner");

    // Parse command line options
    QCommandLineParser parser;
    parser.setApplicationDescription(QApplication::translate(
        "Application description",
        "Convert SVG drawings to clean breeze monochrome icons."));
    parser.addHelpOption();

    QCommandLineOption sizesOption(QStringList() << "s" << "sizes",
                                   QApplication::translate("main",
                                        "Icon sizes, e. g. '16 24 32'"),
                                   QApplication::translate("main", "size ..."));
    parser.addOption(sizesOption);

    QCommandLineOption namesOption(QStringList() << "n" << "names",
                                   QApplication::translate("main",
                                        "Icon names, e. g. 'add-row rotate'"),
                                   QApplication::translate("main", "name ..."));
    parser.addOption(namesOption);

    QCommandLineOption countOption(QStringList() << "c" << "count",
                                   QApplication::translate("main",
                                        "Icon count"),
                                   QApplication::translate("main", "count"));
    parser.addOption(countOption);

    QCommandLineOption classesOption(QStringList() << "classes",
                                     QApplication::translate("main",
                                         "Additional classes, with class name, "
                                         "input color, and output color."),
                                     QApplication::translate("main",
                                         "name color color ..."));
    parser.addOption(classesOption);

    QCommandLineOption noDefaultClassesOption(QStringList() << "no-default-classes",
                                              QApplication::translate("main",
                                                  "Do not add breeze monochrome "
                                                  "classes automatically."));
    parser.addOption(noDefaultClassesOption);

    QCommandLineOption extremeNamesOption(QStringList() << "extreme-class-names",
                                          QApplication::translate("main",
                                              "Allow all technically valid CSS "
                                              "identifiers as class name."));
    parser.addOption(extremeNamesOption);

    QCommandLineOption fixedColorsOption(QStringList() << "f" << "fixed-colors",
                                         QApplication::translate("main",
                                             "Fixed colors, e. g. '#000030'"),
                                         QApplication::translate("main", "color ..."));
    parser.addOption(fixedColorsOption);

    QCommandLineOption proxyColorsOption(QStringList() << "proxy-colors",
                                         QApplication::translate("main",
                                             "Proxy colors, e. g. "
                                             "'blue c ColorScheme-Text t 0.5'"),
                                         QApplication::translate("main", "color ..."));
    parser.addOption(proxyColorsOption);

    QCommandLineOption outputDirOption(QStringList() << "o" << "output-dir",
                                       QApplication::translate("main",
                                           "Output directory, . by default. "
                                           "For every size, a new directory "
                                           "within the output directory is created. "
                                           "Note that this affects only the generated "
                                           "icon files, but not the template file."),
                                       QApplication::translate("main", "path"),
                                       QStringLiteral("./"));
    parser.addOption(outputDirOption);

    QCommandLineOption storeCommandOption(QStringList() << "store-command",
                                          QApplication::translate("main",
                                              "Store the command, which you used "
                                              "to invoke breeze-icon-cleaner, next "
                                              "to the template file."));
    parser.addOption(storeCommandOption);

    parser.process(app);

    // Process command line options, geometry related
    QList<int> sizesList = getSizes(parser.value(sizesOption));
    if (sizesList.isEmpty()) {
        return 1;
    }
    QList<int> offsetsVertical = getVerticalOffsets(sizesList);
    int offsetHorizontal = sizesList.last();

    // Process command line options, file related
    QStringList namesList = getNames(parser.value(namesOption));
    int count = getCount(parser.value(countOption));
    // Need either names list or count
    if ((count <= 0) == (namesList.isEmpty())) {
        qDebug() << "Please specify either --names or --count.";
        return 2;
    }

    QString outputDir;
    if (!namesList.isEmpty()) {
        outputDir = getOutputDir(parser.value(outputDirOption));
        if (outputDir.isNull()) {
            return 8;
        }
    }

    QString fileName = getFileName(parser.positionalArguments());
    if (fileName.isEmpty()) {
        return 3;
    }

    if (parser.isSet(storeCommandOption)) {
        QString commandString;
        for (int i = 0; i < argc; ++i) {
            // Rudimentary escaping with '' if the string contains spaces
            QString arg(argv[i]);
            if (arg.contains(' ')) {
                commandString.append(QStringLiteral("'%1'").arg(arg));
            } else {
                commandString.append(arg);
            }
            commandString.append(' ');
        }

        QFile commandFile(fileName + ".txt");
        if (!commandFile.open(QFile::OpenModeFlag::WriteOnly | QFile::OpenModeFlag::Text)) {
            qDebug() << "Could not open command store file" << fileName + ".txt";
            return 9;
        }
        commandFile.write(commandString.trimmed().toLatin1());
        commandFile.write("\n");
        commandFile.close();
    }

    // Process command line options, color scheme related
    IconColorScheme colorScheme;
    useExtremeCssClassNames = parser.isSet(extremeNamesOption);

    if (!parser.isSet(noDefaultClassesOption)) {
        fillColorSchemeBreeze(colorScheme);
    }

    if (!getClasses(colorScheme, parser.value(classesOption))) {
        return 4;
    }

    if (!getProxyColors(colorScheme, parser.value(proxyColorsOption))) {
        return 5;
    }

    if (!getFixedColors(colorScheme, parser.value(fixedColorsOption))) {
        return 6;
    }

    // Do the work
    if (namesList.isEmpty()) {
        // Output pattern file
        generateTemplate(sizesList, offsetsVertical, count, colorScheme, fileName);
    } else {
        // Clean icons
        // Prepare QSvgRenderer
        QSvgRenderer svgRenderer;
        if (svgRenderer.load(fileName) == false) {
            qDebug() << "Failed to load the input file" << fileName;
            return 7;
        }

        // Generate icon for every name and size.
        // For every name, horizontal offset is increased
        // by offsetHorizontal.
        // For every size, vertical offset is increased
        // by the preceding size.
        int xOffset = 0;
        for (QString name : namesList) {
            if (!name.isNull()) {
                for (int i = 0; i < sizesList.count(); ++i) {
                    // Generate Icon i/name.svg
                    generateIcon(&svgRenderer,
                                QRect(xOffset,
                                    offsetsVertical.at(i),
                                    sizesList.at(i),
                                    sizesList.at(i)),
                                &colorScheme,
                                outputDir + "/" + QString::number(sizesList.at(i)),
                                name + ".svg");
                }
            }
            xOffset += offsetHorizontal;
        }
    }
}

QList<int> getSizes(const QString &sizesArg)
{
    QList<int> sizesList;

    for (QString s : sizesArg.split(' ', Qt::SkipEmptyParts)) {
        bool success;
        int size = s.toInt(&success);
        if (success && size) {
            if (sizesList.isEmpty() || sizesList.last() < size) {
                sizesList.append(size);
            } else {
                qDebug() << "Sizes not ascending.";
                return QList<int>();
            }
        } else {
            qDebug() << "Bad size:" << s;
            return QList<int>();
        }
    }

    if (sizesList.isEmpty()) {
        qDebug() << "No sizes given.";
    }

    return sizesList;
}

QStringList getNames(const QString &namesArg)
{
    QStringList namesList;

    for (QString s : namesArg.split(' ', Qt::SkipEmptyParts)) {
        if (s == QStringLiteral("-")) {
            namesList.append(QString());
        } else if (namesList.contains(s)) {
            qDebug() << "Name given more than once:" << s;
            return QStringList();
        } else {
            namesList.append(s);
        }
    }

    return namesList;
}

int getCount(const QString &countArg)
{
    if (countArg.isNull()) {
        return 0;
    }

    bool success;
    int count = countArg.toInt(&success);
    if (success && count >= 1) {
        return count;
    } else {
        qDebug() << "Bad count:" << countArg;
        return 0;
    }
}

bool getClasses(IconColorScheme &colorScheme, const QString &classesArg)
{
    const QStringList args = classesArg.split(' ', Qt::SkipEmptyParts);
    if ((args.count() % 3) != 0) {
        qDebug() << "--classes arguments not a multiple of 3:" << args;
        return false;
    }

    for (int i = 0; i < args.count(); i += 3) {
        QString className(args.at(i));
        if (!isValidCssClass(className)) {
            return false;
        }
        QColor inputColor(args.at(i + 1));
        if (!inputColor.isValid()) {
            qDebug() << "Invalid input color:" << args.at(i + 1);
            return false;
        }
        QColor outputColor(args.at(i + 2));
        if (!outputColor.isValid()) {
            qDebug() << "Invalid output color:" << args.at(i + 2);
            return false;
        }
        if (!colorScheme.addEntry({inputColor, outputColor, className, 1, IconColorScheme::BreezeClass})) {
            qDebug() << "Input color used more than once:" << args.at(i + 1);
            return false;
        }
    }

    return true;
}

bool getProxyColors(IconColorScheme &colorScheme, const QString &colorsArg)
{
    // Proxy colors are given in the form <input color> <attributes>.
    // Attributes are of form <token> <value>, with this possibilities:
    // c <class name>
    // f <output color>
    // t <transparency>
    // c or f attribute define proxy color type: BreezeClassProxy or FixedColor.
    //
    // Proxy colors are parsed token-wise.
    // After every token, state and have<attribute> store which information
    // is already parsed, and which token is expected next.

    // To remember what token was processed last.
    enum State {
        Empty,                  // Before the first proxy color
        HaveInputColor,         // After the input color token, or after t attribute, no c or f attribute so far
        ExpectOutputColor,      // After "f" token
        ExpectClass,            // After "c" token
        ExpectTransparency,     // After "t" token
        Complete                // After either c or f attribute, and optionally after t attribute
    };

    // To remember which information is already given.
    bool haveOutputColor = false;
    bool haveClass = false;
    bool haveTransparency = false;
    State state = Empty;
    IconColorScheme::Entry proxyColorEntry{Qt::black, Qt::black, QString(),
                                           1, IconColorScheme::FixedColor};

    for (const QString s : colorsArg.split(' ', Qt::SkipEmptyParts)) {
        if (state == Empty) {
            // First element must be input color
            QColor c(s);
            if (c.isValid()) {
                proxyColorEntry.inputColor = c;
                state = HaveInputColor;
            } else {
                qDebug() << "Bad input color for proxy color given:" << s;
                return false;
            }
        } else if (state == HaveInputColor) {
            // Next element must be "c", "f", or "t" token.
            // Note that this state can occur after t attribute given.
            if (s == QStringLiteral("c")) {
                state = ExpectClass;
                proxyColorEntry.type = IconColorScheme::BreezeClassProxy;
            } else if (s == QStringLiteral("f")) {
                state = ExpectOutputColor;
                proxyColorEntry.type = IconColorScheme::FixedColor;
            } else if (s == QStringLiteral("t")) {
                if (haveTransparency) {
                    qDebug() << "Transparency given more than once for same proxy color";
                    return false;
                } else {
                    state = ExpectTransparency;
                }
            } else {
                qDebug() << "Bad attribute token:" << s << "Expected c, f, or t.";
                return false;
            }
        } else if (state == ExpectClass) {
            // Next element must be CSS class name
            if (isValidCssClass(s)) {
                proxyColorEntry.className = s;
                haveClass = true;
                state = Complete;
            } else {
                qDebug() << "Bad class name for proxy color given:" << s;
                return false;
            }
        } else if (state == ExpectOutputColor) {
            // Next element must be color
            QColor c(s);
            if (c.isValid()) {
                proxyColorEntry.outputColor = c;
                haveOutputColor = true;
                state = Complete;
            } else {
                qDebug() << "Bad output color for proxy color given:" << s;
                return false;
            }
        } else if (state == ExpectTransparency) {
            // Next element must be transparency
            bool success;
            double t = s.toDouble(&success);
            if (success && t >= 0 && t <= 1) {
                proxyColorEntry.transparency = t;
                haveTransparency = true;
                state = (haveClass || haveOutputColor) ? Complete : HaveInputColor;
            } else {
                qDebug() << "Bad transparency for proxy color given:" << s;
                return false;
            }
        } else if (state == Complete) {
            // Next element can be next input color or "t" token
            if (s == QStringLiteral("t")) {
                if (haveTransparency) {
                    qDebug() << "Transparency given more than once for same proxy color";
                    return false;
                } else {
                    state = ExpectTransparency;
                }
            } else {
                // Not "t" token, save completed proxy color, and reset
                if (!colorScheme.addEntry(proxyColorEntry)) {
                    qDebug() << "Input color used more than once:" << proxyColorEntry.inputColor;
                    return false;
                }
                proxyColorEntry.transparency = 1;
                haveOutputColor = haveClass = haveTransparency = false;

                // Because it is not a "t" token, it must be next input color.
                QColor c(s);
                if (c.isValid()) {
                    proxyColorEntry.inputColor = c;
                    state = HaveInputColor;
                } else {
                    qDebug() << "Bad input color for proxy color given:" << s;
                    return false;
                }
            }
        }
    }

    // All arguments processed.
    // If proxy colors were given, the last needs to be saved now.
    if (state == Empty) {
        return true;
    } else if (state == Complete) {
        colorScheme.addEntry(proxyColorEntry);
        return true;
    } else {
        qDebug() << "Proxy color incomplete.";
        return false;
    }
}

namespace CssClassRegExp {
    // Based on CSS2.1 Section 4.1.3 Characters and Case - Identifiers
    QString unicode("\\\\[0-9a-fA-F]{1,6}(?:\\r\\n|[ \\n\\r\\t\\f])?");
    QString nonascii("[^\\x{00}-\\x{A0}]");
    QString escape("(?:" + unicode + ")|\\\\[^\\n\\r\\f0-9a-fA-F]");
    QString nmchar("[_a-zA-Z0-9-]|" + nonascii + "|" + escape);
    QString nmstart("[_a-zA-Z]|" + nonascii + "|" + escape);
    QString ident("[-]?(?:" + nmstart + ")(?:" + nmchar + ")*");
    QRegularExpression CssClassRegExp("^" + ident + "$");
    QRegularExpression CssClassSimpleRegExp("^[-]?[_a-zA-Z][_a-zA-Z0-9-]*$");
};

bool isValidCssClass(const QString &className)
{
    if (useExtremeCssClassNames) {
        if (CssClassRegExp::CssClassRegExp.match(className).hasMatch()) {
            return true;
        } else {
            qDebug() << "Invalid class name:" << className;
            return false;
        }
    } else {
        if (CssClassRegExp::CssClassSimpleRegExp.match(className).hasMatch()) {
            return true;
        } else {
            qDebug() << "Invalid class name:" << className << "Try --extreme-class-names ?";
            return false;
        }
    }
}

bool getFixedColors(IconColorScheme &colorScheme, const QString &colorsArg)
{
    for (const QString s : colorsArg.split(' ', Qt::SkipEmptyParts)) {
        QColor c(s);
        if (c.isValid()) {
            if (!colorScheme.addEntry({c, c, QString(), 1, IconColorScheme::FixedColor})) {
                qDebug() << "Fixed color already used as input color:" << s;
                return false;
            }
        } else {
            qDebug() << "Bad color:" << s;
            return false;
        }
    }
    return true;
}

QString getFileName(const QStringList &fileNameArgs)
{
    if (fileNameArgs.count() != 1) {
        qDebug() << "Please specify exactly one file name.";
        return QString();
    } else {
        if (fileNameArgs.first().trimmed().isEmpty()) {
            qDebug() << "Bad file name:" << fileNameArgs.first();
            return QString();
        } else {
            return fileNameArgs.first();
        }
    }
}

QString getOutputDir(const QString &outputDirArg)
{
    QDir dir;
    dir.mkpath(outputDirArg);
    if (dir.cd(outputDirArg)) {
        return dir.path();
    } else {
        qDebug() << "Could not create directory:" << outputDirArg;
        return QString();
    }
}

QList<int> getVerticalOffsets(const QList<int> &sizes)
{
    QList<int> offsetList;

    int lastOffset = 0;
    for (int size : sizes) {
        offsetList.append(lastOffset);
        lastOffset += size;
    }

    return offsetList;
}

void generateTemplate(const QList<int> &sizes,
                      const QList<int> &verticalOffsets, int count,
                      const IconColorScheme &colorScheme,
                      const QString &outputFileName)
{
    // SVG header
    QString fileContent = QStringLiteral(
        "<!DOCTYPE svg>\n"
        "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 %1 %2\">\n")
        .arg(QString::number(sizes.last() * count))
        .arg(QString::number(verticalOffsets.last() + sizes.last()));

    // Add icon squares
    for (int i = 0; i < count; ++i) {
        for (int j = 0; j < sizes.count(); ++j) {
            fileContent.append(generateSquare(sizes.at(j),
                                              sizes.last() * i,
                                              verticalOffsets.at(j),
                                              QStringLiteral("#b4ffb4")));
        }
    }

    // BEGIN Calculate color swatches
    // The BreezeClass and the FixedColor (t = 1) colors make the bottom row and act as index.
    // Between the BreezeClass index and the FixedColor index we make some space.
    // The BreezeClassProxy colors will be arranged above the corresponding BreezeClass swatches.
    // Transparent FixedColors will be arranged above the opaque FixedColor swatches.
    // One row per transparency value, top is least opaque.
    // We assume that there are no duplicates of same class / output color and transparency.
    //
    // We will make a QVector<QVector<IconColorScheme::Entry*>>,
    // where we first populate the bottom row (index [][0]) with intransparent colors.
    // Then we sort the list of the remaining colors, so most opaque comes first.
    // For every color, we search the corresponding index color,
    // and replace the last value above that color with our color.
    // For every new transparency, we add a row of nullptr.

    // Populate bottom row with intransparent colors.
    // BreezeClass list, nullptr, FixedColor list.
    // Collect transparent colors (proxy colors) in another list.
    QVector<const IconColorScheme::Entry*> proxyColors;
    QVector<QVector<const IconColorScheme::Entry*>> swatchesList, fixedColorsIndices;

    for (const IconColorScheme::Entry &entry : colorScheme.allEntries()) {
        if (entry.type == IconColorScheme::BreezeClass) {
            swatchesList.append(QVector<const IconColorScheme::Entry*>() << &entry);
        } else if (entry.type == IconColorScheme::FixedColor
                   && entry.transparency == 1 && entry.inputColor == entry.outputColor) {
            fixedColorsIndices.append(QVector<const IconColorScheme::Entry*>() << &entry);
        } else if (entry.type != IconColorScheme::DraftColor) {
            proxyColors.append(&entry);
        }
    }

    swatchesList.append(QVector<const IconColorScheme::Entry*>(1, nullptr));
    swatchesList.append(fixedColorsIndices);

    // Sort transparent colors, most opaque first
    std::sort(proxyColors.begin(), proxyColors.end(),
              [](const IconColorScheme::Entry* a, const IconColorScheme::Entry* b) {
                  return a->transparency > b->transparency;
              });

    // Append proxy colors to index colors, one transparency per row.
    // For each new transparency, add a row of nullptr,
    // then replace nullptr entries with proxy colors.
    // If index color for a FixedColor does not exist, add one.
    // These index colors are stored in tmpIndexColors.
    qreal lastTransparency = 2;
    QVector<IconColorScheme::Entry> tmpIndexColors;
    for (const IconColorScheme::Entry *proxyColor : qAsConst(proxyColors)) {
        if (lastTransparency != proxyColor->transparency) {
            // Add nullptr row
            for (QVector<const IconColorScheme::Entry*> &column : swatchesList) {
                column.append(nullptr);
            }
            lastTransparency = proxyColor->transparency;
        }

        // Find index color column
        auto rowIt = std::find_if(swatchesList.begin(), swatchesList.end(),
                                  [&proxyColor](const QVector<const IconColorScheme::Entry*> &a){
                                      const IconColorScheme::Entry* indexColor = a.first();
                                      if (!indexColor) {
                                          return false;
                                      } else if (proxyColor->type == IconColorScheme::BreezeClassProxy) {
                                          return indexColor->className == proxyColor->className;
                                      } else {
                                          return indexColor->outputColor == proxyColor->outputColor;
                                      }
                                  });
        // If no index color found, test whether one can be added. (Only possible for FixedColor.)
        if (rowIt == swatchesList.cend()) {
            if (proxyColor->type == IconColorScheme::FixedColor) {
                // Append a nullptr column with the same lenght as the other columns.
                swatchesList.append(QVector<const IconColorScheme::Entry*>(swatchesList.first().count(), nullptr));
                // Then create a temporary entry to act as color index,
                // and set the first entry of the appended list to the temporary entry.
                tmpIndexColors.append({QColor(), proxyColor->outputColor,
                                       QString(), 1, IconColorScheme::FixedColor});
                swatchesList.last().first() = &tmpIndexColors.last();
            } else {
                qDebug() << "Found no class for proxy color with class name" << proxyColor->className;
                return;
            }
        }

        // Add proxy color to found column.
        rowIt->last() = proxyColor;
    }
    // END Calculate color swatches

    // Add color swatches.
    // We have a sparse QVector<QVector<IconColorScheme::Entry*>>,
    // which might contain temporary colors.
    // We skip all temporary colors and all nullptr entries.
    for (int column = 0; column < swatchesList.count(); ++column) {
        for (int row = 0; row < swatchesList.at(column).count(); ++row) {
            const IconColorScheme::Entry *entry = swatchesList.at(column).at(row);
            // Skip unneeded swatches
            if (entry == nullptr) {
                continue;
            } else if (!entry->inputColor.isValid()) {
                continue;
            }

            fileContent.append(generateSquare(4, 5 * column, -6 - 5 * row, entry->inputColor));
        }
    }

    // Finish SVG document
    fileContent.append(QStringLiteral("</svg>\n"));

    // Create output file
    QFile file(outputFileName);
    if (!file.open(QFile::OpenModeFlag::WriteOnly)) {
        qDebug() << "Could not open template file" << outputFileName;
        return;
    }
    file.write(fileContent.toLatin1());
    file.close();
}

QString generateSquare(int size, int offsetX, int offsetY,
                       const QColor &color)
{
    return QStringLiteral(
        "    <path style=\"fill:%4; fill-opacity:1; stroke:none;\" "
        "d=\"M %1 %2 l 0 %3 l %3 0 l 0 -%3 z\" />\n")
        .arg(offsetX).arg(offsetY).arg(size).arg(color.name());
}

void fillColorSchemeBreeze(IconColorScheme &colorScheme)
{
    // Shade Black
    colorScheme.addEntry({QColor(QStringLiteral("#232629")), QColor(QStringLiteral("#232629")),
                          QStringLiteral("ColorScheme-Text"), 1, IconColorScheme::BreezeClass});
    // Icon Red
    colorScheme.addEntry({QColor(QStringLiteral("#da4453")), QColor(QStringLiteral("#da4453")),
                          QStringLiteral("ColorScheme-NegativeText"), 1, IconColorScheme::BreezeClass});
    // Beware Orange
    colorScheme.addEntry({QColor(QStringLiteral("#f67400")), QColor(QStringLiteral("#f67400")),
                          QStringLiteral("ColorScheme-NeutralText"), 1, IconColorScheme::BreezeClass});
    // Plasma Blue
    colorScheme.addEntry({QColor(QStringLiteral("#3daee9")), QColor(QStringLiteral("#3daee9")),
                          QStringLiteral("ColorScheme-Highlight"), 1, IconColorScheme::BreezeClass});
    // Noble Fir
    colorScheme.addEntry({QColor(QStringLiteral("#27ae60")), QColor(QStringLiteral("#27ae60")),
                          QStringLiteral("ColorScheme-PositiveText"), 1, IconColorScheme::BreezeClass});

    // Draft color, as used by generateTemplate()
    colorScheme.addEntry({QColor(QStringLiteral("#b4ffb4")), QColor(QStringLiteral("#b4ffb4")),
                          QString(), 0, IconColorScheme::DraftColor});
}

void generateIcon(QSvgRenderer *renderer,
                  const QRect &clip,
                  const IconColorScheme *colorScheme,
                  const QString &outputFileDir,
                  const QString &outputFileName)
{
    // Set up paint device
    IconPaintDevice dev;
    dev.setClip(clip);
    dev.setIconColorScheme(colorScheme);

    // Paint on the device
    QPainter painter(&dev);
    // BUG Need to set viewBox to same size as clip,
    // otherwise no PainterPaths, but only pixmaps will be painted.
    renderer->setViewBox(QRect(0, 0, clip.width(), clip.height()));
    renderer->render(&painter);
    painter.end();

    // Create output directory
    QDir dir;
    dir.mkdir(outputFileDir);

    // Create output file
    QFile file(outputFileDir + "/" + outputFileName);
    file.open(QFile::OpenModeFlag::WriteOnly);
    dev.save(&file);
    file.close();
}
