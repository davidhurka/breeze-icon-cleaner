QT       += core gui xml svg
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = breeze-icon-cleaner
TEMPLATE = app

SOURCES += src/main.cpp \
           src/IconPaintDevice/iconcolorscheme.cpp \
           src/IconPaintDevice/iconpaintdevice.cpp \
           src/IconPaintDevice/iconpaintengine.cpp
#            src/inputfile.cpp

HEADERS  += src/IconPaintDevice/iconcolorscheme.h \
            src/IconPaintDevice/iconpaintdevice.h \
            src/IconPaintDevice/iconpaintengine.h
#             src/inputfile.h
