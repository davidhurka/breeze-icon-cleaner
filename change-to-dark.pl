#!/usr/bin/perl -pi
use strict;

# No need to change background color or so, breeze-icon-cleaner doesn't generate that.
s/#232629/#eff0f1/;
