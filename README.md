# breeze-icon-cleaner

This program cleans monochrome breeze icons.

The user draws the icon in Inkscape (using only path elements).
This program reads the SVG and collects all path elements.
Then it writes them to another SVG file,
with the correct attributes (color scheme & CSS stuff).

To aid drawing the icon, this program can generate templates,
on which multiple icons in multiple sizes can be drawn at once.
Then this program will separate these icons.

## Basic Usage

To get a template for three icons in two sizes:
```shell
$ breeze-icon-cleaner --sizes '16 22' --count 3 template.svg
```
This will create a template with some light green squares. 3 16px squares, and 3 22px squares.
You can paint your icons directly on top of these squares.

The template will also contain color swatches for all specified classes and colors.
In the bottom row there are all classes, a gap, and all fixed colors.
Above the corresponding swatch in the bottom row, there are all proxy colors.
Proxy colors are ordered by transparency,
where all colors in a row have same transparency, and bottom rows are most opaque.
You can use the swatches to color your icons using the pipette / color dropper tool.

To get the breeze icon files, after painting the icons on the template:
```shell
$ breeze-icon-cleaner --sizes '16 22' --names 'aaa bbb ccc' template.svg
```
This will create icon files aaa.svg, bbb.svg, ccc.svg in folders 16/ and 22/.

You can specify the placeholder name `-` (single dash),
then the corresponding icon in the template file will be skipped.
```shell
$ breeze-icon-cleaner --sizes '16 22' --names '- - ccc' template.svg
```

## Classes

By default, breeze-icon-cleaner maps elements to one of
the 5 monochrome breeze classes.

You can add more classes using the `--classes` option,
by specifying class name, input color, and output color:
```
--classes 'ColorScheme-ViewBackground #fcfcfc #fcfcfc ColorScheme-ButtonFocus blue #3daee9'
```
The input color is the color as you use it in the template file,
the output color is the color which will be added to the stylesheet.

To delete the default 5 classes, use the `--no-default-classes` option.

By default, class names may contain the characters `[_a-zA-Z0-9-]`.
If you need more, use the `--extreme-class-names` option.
Note that you must use the same spelling every time.
Note that you still can not use whitespace.

## Proxy colors

breeze-icon-cleaner allows to define proxy colors,
with which you can use transparency and fixed colors.

You can add proxy colors using the `--proxy-colors` option,
by specifying input color, class name or output color, and optionally transparency.
```
--proxy-colors 'yellow c ColorScheme-Text t 0.5 #0000ff f #000000'
```
The `c` attribute defines to which class the element will be linked.
The `f` attribute defines a fixed output color.
The `t` attribute defines output transparency, in range 0 (invisible) ... 1 (opaque).
You must provide either the `c` attribute or the `f` attribute.

Do not specify two proxy colors with same class / output color and transparency.

If input color and fixed output color shall be the same,
and transparency shall be opaque, you can use the shorthand `--fixed-colors` or `-f`.
```
-f 'red blue #cOffee'
```

## Additional styles

breeze-icon-cleaner is targeted at purely monochrome breeze icons.
If you need to add additional styles like gradients,
you need to edit the output file manually.
In that case, the `--fixed-colors` option may be handy.
Note that it might be easier to avoid breeze-icon-cleaner completely.

## Additional options

`--store-command` stores your command line command in a file next to
the template file, e. g. in template.svg.txt.
This is useful to remember which colors you added to the color scheme.

`--output-dir` allows to change the directory in which the icon output
directory is created. E. g. with `--output-dir /tmp/`, icons are generated
in /tmp/16/, /tmp/22/, ...
This does not affect the path to the template file.

## Breeze Dark Icons

Currently, the breeze-icons repository contains an icons-dark/ directory.
The icons in there are usually the same as in icons/, with a slight difference:
Their color scheme contains a bright color for ColorScheme-Text.
To convert breeze icon files as output from breeze-icon-cleaner,
there is a small script in this repository: change-to-dark.pl.
To use it, copy the output files to a new directory (e. g. icons-dark/) and call:
```shell
$ ./change-to-dark.pl icons-dark/*/*.svg
```

